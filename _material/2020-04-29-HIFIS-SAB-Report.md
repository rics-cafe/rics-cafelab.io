---
layout: post
title: "HIFIS SAB Report"
categories: [Presentation]
authors: Patrick
external_link: https://desycloud.desy.de/index.php/s/ZSEkwmZRdCipFkw 
---
   Friday this week, we'll welcome our three new colleagues: Franz,
   David and Tim. Franz and David will work with Philipp on HIP
   "Imaging" and Tim will bridge between Cloud and HPC (HIFIS) and will
   work with Michael and our HPC people.
