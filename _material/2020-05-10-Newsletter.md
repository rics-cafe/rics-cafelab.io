---
layout: post
title: "RIC Report 2020-05-10"
categories: [Newsletter]
author: Patrick
---

## New People
   Friday this week, we'll welcome our three new colleagues: Franz,
   David and Tim. Franz and David will work with Philipp on HIP
   "Imaging" and Tim will bridge between Cloud and HPC (HIFIS) and will
   work with Michael and our HPC people.

## Helmholtz Platforms

### HIP
* HIP is now seriously starting up at DESY. On our side,
       Philipps two new colleagues (Franz and David) will begin
       their job this Friday (15/5).
* On the scientific side (Christian Schroer), the management is
       establishing itself. The coordinator is Sara Krause-Solberg.
       Good news is that the administrative manager turned out to be Knut
       Sander. Sara and Knut will be soon very busy evaluating and selecting
       proposals on Imaging. It is envisioned that Philipp and his
       team will guide scientists, with accepted proposals, on how to
       share software and algorithms efficiently with other HIP groups
       or Helmholtz Centres.

### HIFIS

#### Administratively
                
   End of April (29/4) Uwe brilliantly organised and ran the first
   HIFIS Scientific Advisory Board (SAB) meeting. By accident, lots of
   old friends were among the external advisors, like Isabel, Tiziana,
   Davide Salomoni and many more. Consequently, Uwe and myself got
   some preliminary unofficial remarks from our advisors. (Very
   interesting). Have a look at the slides, the HIFIS team prepared
   for the [board meeting](https://desycloud.desy.de/index.php/s/ZSEkwmZRdCipFkw)

#### Something more technical
                        
   The various HIFIS clusters decided to consolidate their different
   documentation platforms (and hopefully their web pages too), using a
   gitlab (at HZDR). With that, they would follow the lead of the HIFIS
   Software Cluster. Sometimes it is just breath-taking how fast the
   German scientific community is entering the digital century.

### eXtreme DataCloud is coming to an end.
   Paul is currently busy preparing for the final review of the
   eXtreme DataCloud (XDC) project. He kindly volunteered to take over
   my job as work package leader. He will present a DEMO on how the
   European XFEL is using storage events to steer data transfers and
   start computing jobs on arriving data segments. Hopefully we can
   make that demo available after the review. The challenging bit will
   be to confuse the reviewers sufficiently to hide the fact that not
   much happened in that project for the last year. But I can't
   imagine anyone better for that job than Dr. Millar.

### Gitlab on our Cloud Management Infrastructure

#### Starting with good news
                        
   Michael and Johannes prepared the necessary templates to create
   'gitlab' instances on our Cloud Infrastructure by (more or less)
   just pressing a button. (HEAT templates) The first instance will be
   hopefully for the CTA organization, in case they agree. But more
   customers are already kicking their heels. Some of them were
   inspired by the eosc-pan-git instance, Michael set up ages ago.
   It's funny to remember how Michael was continuously verbally
   attacked when he first advocated to run such a service for the DESY
   scientific communities on our OpenStack. The same people are now
   advertising gitlab wherever and whenever they can. But that's
   actually good news, putting aside that in most European Scientific
   labs, gitlab services are as common as electricity. That was, by the
   way, one of the SAB advising remarks :-).

#### Less good news
              
   The CEPH glitch, which started end of last week, and which rendered
   our external Open Stack useless for days, indicated that we might need
   some more experience in the one of other technology we use, which
   btw, Yves is telling my all the time, and I refused to believe. (Silly
   me)

## Report from the FGL and MF meeting

### More hiring
* Secretariat: During this and the following week, Volker, Sabine
       and some more colleagues will start interviewing candidates for
       the vacant secretariat position. So hopefully Sabine will get
       some help soon.
* dCache: Good news for dCache people: Marina got a permanent contract.

### Report from a meeting Volker had with the DFN
* They are still looking for a video portal for 'Education'. Must
      be cheap. No decision yet.
* In general the DFN regards Zoom as the only solution for large
       scale video conferencing, matching privacy and performance
       requirements, especially as the Zoom people are willing to
       complying with national privacy regulations. However Zoom
       prefers to negotiate with institutions directly and not with
       national NRENs like the DFN. (Exception: our friends from NorduNet)
* The bandwidth bottleneck between the Telekom and the DFN seems
       to be resolved. Unfortunately the peering solution is not
       coming for free. BTW, the DFN peering connection to Amazon
       (2*20GBit) is payed for by Amazon.

### On the HGF Incubator
   There will be a general Incubator meeting mid of November. We are
   invited to contribute with topics till end of May.

### On the DESY IdP.
   Peter upgraded our IdP to a supported version and in consequence we
   are no longer blacklisted by the DFN. So, we can use the EGI GocDB
   again w/o certificate. However, until end of this year we have to
   upgrade our Shibboleth to a 4.x version, otherwise the same game
   starts all over.

### On Mattermost
   People are happy with our central Mattermost. Groups like ILC, the HESS
   telescope and others are planning to migrate from their current
   system to Mattermost. The system will be declared 'production' as
   soon as the directorate confirms.

### On COVID-19
   The DESY corona task force is finishing an 'operational concept'
   (Betriebskonzept) to start up DESY again, including questions on
   who will need to wear masks and how the canteen can be reopened
   again. The plan is to start-up the accelerator facilities midterm
   but w/o guests. My very personal opinion is that we should expect
   some more weeks in home office.

