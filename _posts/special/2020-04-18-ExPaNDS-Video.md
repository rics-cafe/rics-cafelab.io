---
layout: post
title: "ExPaNDS Info Movies"
categories: [Material,Presentation]
author: "ExPaNDS WP2"
external_link: "https://s3.eu-central-1.amazonaws.com/ric.solutions/videos/RIC-ExPaNDS-Video-2.mp4"
external_link_2: "https://s3.eu-central-1.amazonaws.com/ric.solutions/videos/RIC-ExPaNDS-Video-1.mp4"
external_linkx1: "https://s3.eu-central-1.amazonaws.com/ric.solutions/videos/ExPaNDS_Video-1.mp4"
external_linkx2: "https://s3.eu-central-1.amazonaws.com/ric.solutions/videos/ExPaNDS_Video-2.mp4"
---

## European Open Science Cloud opportunity

ExPaNDS and PaNOSC see EOSC as an opportunity to generalise the adoption of FAIR data practices at our PaN RIs that will enable data sharing, the reuse across a wider community and the provisioning of services for remote data analysis. We will map all relevant data catalogues to ensure that our scientific research communities have access to both the raw data collected that is linked to the experiments at their facility, and the relevant peer review articles produced as a direct result of their usage.

<br>
<br>
### ExPaNDS First Infographics
<br>
<div style="text-align: center; border: solid 1px black; box-shadow: 4px 4px #ccc">
<video width="640" height="480" controls>
  <source src="{{ page.external_link_2 }}" type="video/mp4">
Your browser does not support the video tag.
</video>
</div>
<br>
<br>
### ExPaNDS Second Infographics
<br>
<div style="text-align: center; border: solid 1px black; box-shadow: 4px 4px #ccc">
<video width="640" height="480" controls>
  <source src="{{ page.external_link }}" type="video/mp4">
Your browser does not support the video tag.
</video>
</div>

The goal of our projects and the EOSC is to make data from publicly funded research in Europe Findable, Accessible, Interoperable and Reusable (FAIR). Fostering FAIR Data Practices will allow other facilities and their users to benefit from the valuable data produced thanks to our light and neutron sources to help combat 21st century global problems. By reusing data, it is possible to plan experiments and simulations that will produce more than was originally imagined.


