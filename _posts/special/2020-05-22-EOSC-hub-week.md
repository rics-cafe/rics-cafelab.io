---
layout: post
title: "Bits and pieces from EOSC-hub Week 2020"
categories: [Special Issues]
author: Sophie
---


[![](https://www.eosc-hub.eu/sites/default/files/styles/newsresponsive/public/bigstock-technology%20network-175029769.jpg?itok=k22IngUh)](https://www.eosc-hub.eu/events/eosc-hub-week-2020-goes-virtual)
_Credits: [eosc-hub.eu](https://www.eosc-hub.eu/events/eosc-hub-week-2020-goes-virtual)_

From 18th to 20th of May, the __EOSC-hub week__ took place as a series of webconferences.

There were loads of interesting presentations giving insights into the progress of EOSC, examples of concrete applications for different science communities and ongoing works from our friends in other EOSC-related projects like for example [XDC](/projects/xdc).

The presentations, and soon the recordings, are all openly accessible [here](https://www.eosc-hub.eu/eosc-hub-week-2020/agenda).

# What will EOSC be by the end of 2020?
The first day was an __EOSC Consultation Day__. With the EOSC Legal Entity set to start in Jan. '21, a series of structuring documents are being drawn up:

- __The 'Iron lady' document on EOSC Sustainability__ which will contain actual recommendations on how the EOSC can be sustained, including business model, costing exercice, risk assessment, legal advice, etc;
- The [EOSC Interoperability Framework](https://www.eoscsecretariat.eu/sites/default/files/eosc-interoperability-framework-v1.0.pdf), which proposes a set of recommandations and challenges to make the 'I' in FAIR a reality for EOSC;
- The __PID Policy__ [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3780423.svg)](https://doi.org/10.5281/zenodo.3780423), which, with the same concern for FAIR, exposes principles and guidelines for persistent identifiers to be used by EOSC services and data providers;
- The [FAIR metrics](https://repository.eoscsecretariat.eu/index.php/s/C3a5WkpsFHL6GD3#pdfviewer), explaining how the assessment of FAIRness of data and software will be made (or to be more precise, on what works from RDA and FAIRsFAIR project it will build);
- The [Rules of Participation](https://repository.eoscsecretariat.eu/index.php/s/QWd7tZ7xSWJsesn#pdfviewer) to the EOSC for users and content providers;
- A report on __AAI__ which will be available in the coming weeks, Patrick might know more about this.

Once they're finished by the end of the year, all these documents will represent what EOSC 1.0 is, for the launch of the __legal EOSC association__. Indeed, as of 2021, EOSC will be an 'AISBL', which is the French acronym for _Association internationale sans but lucratif_, International Non-Profit Organization. This legal entity will allow the EOSC, among other things, to sign a partnership agreement with the European Commission and thus highly influence the __Strategic Research & Innovation Agenda__ (SRIA). These are great news for the sustainability of the EOSC.

# And after 2020?
EOSC 1.0 is currently being built by projects like EOSC-hub, OpenAIRE, FREYA, OCRE, EOSC-Enhance, let's call them the 'builders'. They provide the portal and the underlying structure with e.g. AAI and monitoring, they on-board new services and can also provide compute resources. Then, there are those projects who feed the EOSC with content: data, analysis pipelines, software, training material, etc. These are projects like ESCAPE, PaNOSC, ExPaNDS and all EOSC-related H2020 projects that link EOSC to a growing number of researchers. Let's call them the 'feeders'. But of course all these projects have - inherently - end dates!

Who will take care of EOSC 2.0 then?
- Next 'builders' will come from the INFRA-EOSC 03 and 07 calls which are still open (until mid-June) and to which Patrick is working really hard to get us in. The winning projects will start in __Jan. 2021__.
- For 'feeders' there is more time because H2020 projects will still deliver content to the EOSC until 2022. New 'feeders' will be added to the big table of EOSC-related projects with to the __Horizon Europe programme__. Its outlines will be discussed in June with the Member States and in September during the 'Research & Innovation days'. The final budget for Horizon Europe is not set yet, and the first calls will be launched already in __Q1 2021__.

The __key messages for the EOSC 2.0__ I heard were:
- get better at showcasing EOSC's added-value to the scientists;
- focus on __data__ now and not so much on services;
- __work much more with the research communities__;
> Indeed - and the attendees to the event were a live confirmation - EOSC is still struggling to tell the __researchers__ that they are its __main stakeholders__. All current discussions mostly involve those who are building the EOSC, when they should involve those who will actually use it. Hence the utmost importance of the thematic projects and the 'feeding side' to really involve scientists.
__This should be in our favor in the calls to come.__

- be as flexible as possible, we don't know what tomorrow holds.

To keep up with EOSC and exchange with the community on common challenges, there is a 'Liaison Platform' and several 'Interest Groups' where everyone can subscribe.
[![](https://www.eoscsecretariat.eu/sites/default/files/styles/900x400/public/eosc_sec_twitter_3.jpg?itok=q932vJIG)](https://www.eoscsecretariat.eu/eosc-liaison-platform)
_Credits: [eoscsecretariat.eu](https://www.eoscsecretariat.eu)_

# And now really interesting stuff others do!
There were nice examples of the use of EOSC-hub's resources in different research contexts. They all have fancy analyses pipelines that can be run on federated cloud resources, sometimes even including linked open data catalogues. There is:
- __WeNMR__ and their many analysis services for __Biological Sciences__. FYI, they are very keen on the udocker developed by INDIGO Data Cloud to be able to use dockers without being root. [Link to presentation](https://confluence.egi.eu/download/attachments/68223757/2020-05-EOSC-WeNMR.pptx?version=1&modificationDate=1589785382974&api=v2).
- __DODAS__ with its on-demand container-based clusters for analysis services for the __HEP community__. Incidentally, after CMS, Virgo and Fermi they are now working on synergies with [ESCAPE](/projects/05-escape) and WLCG. [Link to presentation](https://confluence.egi.eu/download/attachments/68223757/DODAS-EOSC-Week_Spiga.pdf?version=1&modificationDate=1589828853452&api=v2).
- __ECAS__ and its Python environment for data analysis with a JupyterJub instance for __Environmental Science__. [Link to presentation](https://confluence.egi.eu/download/attachments/68223757/EOSC-hub_week_2020_F_Antonio-ECAS_A_data_science_environment_for_climate_change_in_the_EGI_federated_infrastructure.pptx?version=1&modificationDate=1589819678984&api=v2).
- __CLARIN__ and its own __Humanities and Social Sciences__ EOSC already in service! [Link to presentation](https://confluence.egi.eu/download/attachments/68223757/04_CLARIN_slides.pdf?version=1&modificationDate=1589875737017&api=v2).
- __DEEP HybridDataCloud__ and its deep-learning pipelines, notably for image processing. See demo:
<div style="text-align: center">
<iframe width="740" height="420" class="infographic" src="https://www.youtube.com/embed/IXSElvQ17lA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
_Credits: [DEEP HybridDataCloud](https://deep-hybrid-datacloud.eu/)_

Finally, the so-called INFRA-EOSC-5b projects are from the same call as ExPaNDS, so we know them very well. Or so I thought! They are focused on paving the way for the EOSC among their respective regions, for example aligning policies and promoting FAIR. But, they also do concrete technical stuff which could be very interesting for us:
- [EOSC Nordic](https://www.eosc-nordic.eu/) started using PIDs for their services, which is quite convenient because it greps the location (url) which you can update when you move your service. They are thinking on adding other metadata, for which there is no standard yet. They are also developing an __automated assessment of FAIR maturity__ tool to be used for services. [Link to presentation](https://docs.google.com/presentation/d/1Fb8-S8w4RJLCl_5Pbs2AYyOk9DOZz8Ra9eoaJ4Q8PIQ/edit#slide=id.p).
- [EOSC Synergy](https://www.eosc-synergy.eu/) is proposing a __Software Quality Assurance aaS__ and is delivering badges based on the open-source Badgr platform. [Link to presentation](https://confluence.egi.eu/download/attachments/68223757/EOSC-Synergy-SQA-eosc-conference.pptx.pdf?version=1&modificationDate=1589903762419&api=v2).
- [EOSC Pillar](https://www.eosc-pillar.eu/) is __federating several existing community data catalogues__ and national repositories, very much like what we want to do in ExPaNDS and PaNOSC! They are using EUDAT's B2Find metadata harvester. [Link to presentation](https://confluence.egi.eu/download/attachments/68223757/EOSC-Pillar_Innovation.pdf?version=1&modificationDate=1589976914293&api=v2).
- [Ni4OS](https://ni4os.eu/) has already developed a __training platform__ with lots of content on e.g. open science and FitSM which is a light weight service management system. They are using the [BigBlueButton](https://bigbluebutton.org/) webinar system. They also propose a TRL equivalent for EOSC integration level, EIL, that could be used as a KPI for services that want to be onboarded into EOSC. [Link to presentation](https://confluence.egi.eu/download/attachments/68223757/NI4OS-Europe_EOSC-Hub_week_Reg_Innov_Workshop_20_05_2020.pptx?version=1&modificationDate=1589962348108&api=v2).

Some are worth digging into, oder?
