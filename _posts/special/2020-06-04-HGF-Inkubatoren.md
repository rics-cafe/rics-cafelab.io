---
layout: post
title: "HGF Inkubator Meeting"
categories: [Material,Presentation]
author: "HIFIS"
external_link_HIFIS: "https://desycloud.desy.de/index.php/s/7RzZXLqrnHn5cD9"
external_link_HIP:   "https://desycloud.desy.de/index.php/s/z7kfZ7iCo2LbdR2"
external_link_HIDA:  "https://desycloud.desy.de/index.php/s/jZDGFAD6nrPfQYb"
external_link_HMC:   "https://desycloud.desy.de/index.php/s/AqXBbnDMjwFZJc8"
---

### <a href="https://desycloud.desy.de/index.php/s/7RzZXLqrnHn5cD9">The HIFIS Platform Short Report</a>
### <a href="https://desycloud.desy.de/index.php/s/z7kfZ7iCo2LbdR2">The HIP Platform Short Report</a>
### <a href="https://desycloud.desy.de/index.php/s/jZDGFAD6nrPfQYb">The HIDA Platform Short Report</a>
### <a href="https://desycloud.desy.de/index.php/s/AqXBbnDMjwFZJc8">The HMC Platform Short Report</a>


