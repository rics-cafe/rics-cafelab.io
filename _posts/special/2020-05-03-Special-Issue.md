---
layout: post 
title: "Introduction into the EOSC"
categories: [Special Issues]
author: Patrick
---

# EU Glossery

## The Federated Infrastructures

### EGI Foundation
The EGI Foundation (also known as EGI.eu) coordinates the EGI infrastructure on behalf of the participants of the EGI Council: national e-infrastructures and European Intergovernmental Research Organisations (EIROs).

The EGI Foundation is not-for-profit and was established in Amsterdam in 2010 under Dutch law. The foundation is led by Tiziana Ferrari (Managing Director) and is governed by the EGI Council. Day-to-day operations are supervised by the Executive Board.

BTW : Volker is member of the EGI Council and Patrick member of the EGI Executive Board.

[Further Reading](https://www.egi.eu/about/)

### EUDAT
The EUDAT Collaborative Data Infrastructure (or EUDAT CDI) is one of the largest infrastructures of integrated data services and resources supporting research in Europe. It is sustained by a network of more than 20 European research organisations, data and computing centres that on September 2016 have signed an agreement to maintain the EUDAT CDI for the next 10 years and in 2018 have supported the establishment of the limited liability company, EUDAT Ltd.

This infrastructure and its services have been developed in close collaboration with over 50 research communities spanning across many different scientific disciplines and involved at all stage of the design process.

[Further reading](https://eudat.eu/eudat-cdi)

## The Software Projects

* INDIGO-DataCloud
* Extreme DataCloud (XDC)


## EOSC (The European Open Science Cloud)

![EOSC and HIFIS](/assets/images/EOSC-Timeline.png)
![EOSC and HIFIS](/assets/images/EU-HIFIS-small.png)

[The EOSC Declaration](/assets/documents/eosc_declaration.pdf)

