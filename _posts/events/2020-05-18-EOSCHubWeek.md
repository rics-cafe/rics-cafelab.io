---
layout: post
title: "Annual EOSC Hub Week"
categories: [Event]
author: Patrick
external_link: https://www.eosc-hub.eu/events/eosc-hub-week-2020-goes-virtual
status: "Ended"
agenda: https://www.eosc-hub.eu/eosc-hub-week-2020/agenda
real_date: "18 - 20 May, 2020"
---
