---
layout: post
title: "PaN EOSC Symposium"
categories: [Event]
author: Patrick
external_link: https://indico.eli-beams.eu/event/376/
real_date: "9 Nov, 2020"
---
The Extreme-Light-Infrastructure (ELI), the Photon and Neutron Open Science Cloud  (PaNOSC) and the European Open Science Cloud (EOSC) Photon and Neutron Data Service (ExPaNDS) projects are jointly organizing "Photon and Neutron EOSC symposium" that will be held on 9 November 2020 at ELI Beamlines in the outskirts of Prague in Dolní Břežany, Czech Republic.
