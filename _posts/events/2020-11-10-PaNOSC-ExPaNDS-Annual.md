---
layout: post
title: "PaNOSC and ExPaNDS Annual Meeting"
categories: [Event]
author: Patrick
external_link: https://indico.eli-beams.eu/event/369/ 
real_date: "10 - 11 Nov, 2020"
---
The Extreme-Light-Infrastructure (ELI) is pleased to invite you to attend  the annual meeting of the Photon and Neutron Open Science Cloud  (PaNOSC) and the European Open Science Cloud (EOSC) Photon and Neutron Data Service (ExPaNDS) projects that will be held on 9-11 November 2020 at ELI Beamlines in the outskirts of Prague in Dolní Břežany, Czech Republic.
