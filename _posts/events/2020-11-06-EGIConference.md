---
layout: post
title: "Annual EGI Conference"
categories: [Event]
author: Patrick
agenda: https://indico.egi.eu/event/5000/program
external_link: https://indico.egi.eu/event/5000/overview
status: "Call for Abstracts"
real_date: "2 - 4 Nov, 2020"
---
Annual EGI Event. Very likely in Amsterdam. This is event is important for activities around EU and EOSC projects.
