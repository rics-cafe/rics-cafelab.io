---
layout: post
title: "Annual dCache WS"
categories: [Event]
author: Patrick
status: "Finished"
real_date: "26/27 May, 2020"
external_link: https://indico.desy.de/indico/event/25462/
---

Once a year dCache developers, sysadmins and friends from around the world meet to exchange news and best practices and gossip. In the past those meeting were held in beautiful cities around the world like Amsterdam, Madrid, .. Taipei, Hamburg and many more. Due to the current situation, this years meeting was held as VC only, at 26/27 May in the afternoon hours.

# Meeting details

[Agenda and Slides]({{ page.agenda }})

<br>

# Video Material

<div class="grid-container">
{% for vid in site.data.dcache-ws-14 %}
  <div style=" border-top: solid 1px gray;">
    <h1>{{ vid.title }} </h1>
       <video width="350" controls>
        <source src="{{ vid.video }}" type="video/mp4">
        Your browser does not support the video tag.
       </video>
    <div style="display: inline-block; ">
    {{ vid.description }}
    </div>
  </div>
{% endfor %}
</div>
