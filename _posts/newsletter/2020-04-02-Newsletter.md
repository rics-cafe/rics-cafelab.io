---
layout: post
title: "RIC Report 2020-04-02"
categories: [Newsletter]
author: Patrick
---

## The Team
### New team member : Carsten
   Frist and most importantly let's welcome Carsten Heidmann. As
   announced last week he started yesterday, 1/4/2020. He is
   responsible for building the HIFIS technical cloud platform, aka
   Marketplace,Portal together with Femi and colleagues from HZB
   (Berlin), FZJ(Juelich), KIT(Karlsruhe) and other members of the
   HIFIS cloud cluster. Right now he is moving from Karlsruhe to
   Hamburg and as far as I understand, he is still looking for an
   apartment. So any help appreciated.

## Odds and ends from the FGL Meeting
{% if page.internal == "true" %}
* PETRA III is in standby mode and is offering Covid-19 related
       proposals a fast track to beam time [1]. The amount of data
       which will be produced is not clear yet, but Birgit reported
       that DOT can handle whatever there might come.
* The Mattermost system was approved by our Directorate, as you
       already got from Martin's e-mail from about 12:03 today, titled
       "Messaging-Service".
* For those of you, using the Deutsche Telekom to dial into DESY,
       Kars pointed us to an interesting report [1] published by
       Heise.
* On Gitlab for CTA: Some confusion was created on who actually
       was requesting the Gitlab service. (Zeuthen CTA or the CTA
       Organization, CTAO). Volker made that bit 'chefsache' and will
       investigate. Meanwhile Birgit is further negotiating the options
       we have in terms of AAI and scalability.
* DESY is slowly moving away from ISDN to SIP. Kars elaborated a
       little bit on that procedure but I must admit that all the
       details completely slipped my mind. (so many funny words!).
* For more info on the FGL meeting please refer to Birgits e-mail
       from yesterday to "Scientific Computing".

## Some remarks on the 'gitlab' system for CTA(O)
   If we'll manage to get CTA [4] as a customer for a Gitlab, operated
   within our Cloud system, with a modern AAI and fully scalable,
   that would be a great reward for the work Michael and Yves team
   (Marcus,Stefan, Johannes ..) and the network people did for the
   last 2 years in building a professional cloud infrastructure.
   Furthermore, we could use this example as propaganda and template
   for new customers with similar requirements. Hopefully we are not
   spoiling this opportunity.
   BTW: Paul and Aleem are already working
   with CTAO and many more e-infrastructures through the ESCAPE [5]
   project, building a European wide Data Lake.

## Home Office Reminder
   Please update your office location (Home or DESY) for April in the
   INDICO [3] system. Volker asked us to do this once a month.

{% else %}
 This information is not publicly available.
{% endif %}

## Hiring Process
   We are progressing. There are a lots of very
   talented candidates applying for the HIP positions.
   I'll keep you updated on any decision made.


1. [COVID](https://photon-science.desy.de/users_area/fast_track_access_for_covid_19/index_eng.html)
2. [TELCOM-DFN](https://www.heise.de/newsticker/meldung/Deutsches-Forschungsnetz-und-Telekom-Peeren-in-Zeiten-von-Corona-4694172.html)
3. [INDICO Home Office Declaration](https://indico.desy.de/indico/category/721/)
4. [CTA O](https://www.cta-observatory.org/)
5. [ESCAPE Project](https://projectescape.eu/)
