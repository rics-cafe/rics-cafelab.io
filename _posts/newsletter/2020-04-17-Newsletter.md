---
layout: post
title: "RIC Report 2020-04-17"
categories: [Newsletter]
author: Patrick
---
## Reminder : RIC Team video meeting Monday 10:00

### EU Project Reporting (Timesheets)
   Yesterday some of you received timesheets from our EU office (Petra
   Ullmann). At the end of this e-mail, I'm summarising the procedures
   required to respond to that e-mail.

# Hiring for HIFIS and HIP, etc are ongoing with the following update:
* The HIP DESY Team Lead position is now filled and I'm happy to
       tell you that Philipp got it.
* More on HIP: This week we are finalising interviews for
       Philipps two new HIP colleagues. There have been about 30
       applicants, with a lot of good candidates.
* We are starting interviews for the following positions next week:
   * HIFIS: AAI and Storage, in Peter vdR's group
   * HIFIS: HPC, Cloud technologies in our group.
   * PaNOSC/ExPaNDS: in Yves group (non permanent)

# Report from the FGL
 -------------------
## DESY wide Mattermost service
                            
* The DESY wide Mattermost service was officially made available
       yesterday, for DESY people only. A sufficiently high number of
       licences was bought (500) and will be extended as needed. DESY
       PR is planing to use MM to keep people entertained in home
       office by offering a platform to share personal and hopefully
       extremely disgusting pictures.

## Video Systems
              
* **Cisco**: After the Cisco upgrade to 3 hardware units, the system
       seems to behave properly. Peak usage was observed with about 35
       parallel meetings and 130+ participants in total.
* **Jitsi**: Yves and his team are currently evaluating the Jitsi [1]
       video service. They managed to get the service scaling nicely
       on the our Cloud Platform. Proper authentication is now looked
       into. Heads Up : Yves may contact Micheal, Femi and/or Paul for
       help.
* **Zoom**: Within Helmholtz and partially in the Press, there have
       been enthusiastic discussions on how safe Zoom is for our
       scientific purposes. As a result, our colleagues from Helmholtz
       CISPA [2] collected general information available and composed
       a small summary of facts[3].

## Other services and stuff
                        
* VPN and the SSH tunnels seem to behave properly and Kars
       reported 500+ and 600+ peak users resp.
* There are reports that particular batches of SSDs, due to a
       firmware problem, stop working after awhile and can't get
       reactivated ever. Yves people and our colleagues in Zeuthen are
       trying to identify those devices in our computer centers, to
       fix the problem before it's too late. I'll spare you the
       details as RIC is not operating storage hardware. However, in
       case you are interested please contact Yves or his team.
* The discussion with CTA-Organization on a Gitlab services
       shared between Zeuthen and Hamburg is still ongoing. Currently
       discussed is the extend of the Licence[4] , 4 USD or 19 USD.

# References
1. [JITSI](http://Jitsi.org)
2. [CISPA HGF](https://cispa.saarland/de/)
3. [CISPA on Zoom](https://desycloud.desy.de/index.php/s/Q796qZ2imAzs5ZX)
4. [GITLAB Pricing](https://about.gitlab.com/pricing/#self-managed)

# EU Project Members only
{% if page.internal == "true" %}
Yesterday you received your regular time sheets when partially
       working for an EU project. You usually check, sign and put them
       into my mailbox. This time the procedure slightly changed:

As usual, please check the timesheets carefully and make sure,
       the information is correct. In particular make sure that days
       you travelled for non EU projects are not charged on the
       project in your timesheets.

### In case you are working from Home and you have a printer and scanner available
                                                                              
* Please print the timesheets, sign them as usual and scan them
       back in.
* Keep the original signed document and put it into my physical
       mailbox at DESY as soon as you get to DESY next time. (if ever)
* Send the scanned copy back to Petra and (Very importantly) with
       CC to myself, as I have to do the same.

### In case you are working from Home and you don't have a printer and scanner available
                                                                                    
* We are actually asked to contact Petra in that case, but I
       think it's fine that you send the unsigned TS back to Petra and
       in this reply to declare that the content of the timesheets is
       correct.
* However, as soon as being back to DESY you have to print, sign
       and put the original into my mailbox.

### In case you are at DESY
                
* Please sign the timesheets, scan them and put the original into
       my physical mailbox.
* Please send me the scanned copy, as I have to approve them.

### In case you are confused
                
* Call me and don't do anything stupid.
{% else %}
This information is not publicly available.
{% endif %}

