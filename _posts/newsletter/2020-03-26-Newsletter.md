---
layout: post
title: "RIC Report 2020-03-26"
categories: [Newsletter]
author: Patrick
---
 
## Selected topics from this weeks FGL

{% if page.internal == "true" %}
 
* As a result of our famous social distancing, colleagues from
       "networking" are observing increasing stress on the Cisco
       meeting system. Consequently, they are upgrading and extending
       its hardware infrastructure. However, it is advised to avoid
       video streaming, especially in the morning around 11:00 +- .
* Surprisingly, the "Desy Open Day" as well as the annual "Summer
       Student Program" were cancelled.
* DESY IT was asked to provide a chat system for one of our
       customers. The selection process is ongoing. Mattermost is
       still on the short list. Yves is suggesting to run the new
       system on our cloud infrastructure. Seems we are approaching
       the 21 century.
* The CTA [10] organisation (Zeuthen CTAO) is asking for a
       production gitlab with all the bells and whistles. Birgit is
       negotiating the details with the customer.

{% else %}
   Unfortunately, this section is only avaible from within DESY.
{% endif %}

## RIC Communication Infrastructure
   Sophie and myself are working on the RIC communication
   infrastructure. We could reserve the domain
   
       ric.solutions  

   so please use

       team@ric.solutions

   to send messages to the RIC team and

       office@ric.solutions

   to send messages to the RIC office; hence the name.

   On a similar note: Renate Roude was so kind to give us some
   inside into the "Art of web design with ZMS", as Sophie is
   designing the RIC web pages and Philipp the pages for the DESY
   Campus-wide AI initiative. We'll keep you updated on
   our progress.

## Hiring and more

### Helmholtz Imaging Platform, HIP [1]
   DESY IT is hosting one of the core service teams for HIP, composed
   of a team lead and two scientific algorithm and software
   developers. A candidate for the management position was already
   selected last week and the interviews for the remaining two future
   colleagues will take place next week.

### Helmholtz Service Platform, HIFS [2]

DESY is one of the leading partners within the HIFIS platform, composed 
of 11 HGF Centers. DESY IT got quite some money from this platform,
from which we already hired:
* Uwe, the HIFIS coordinaton
* Femi, the HIFIS system designer and developer
* Carsten, the head of the Technical Platform
  * Carsten will start April 1, but is already listing.
* Another position is still pending [3].

Outside of our group, HIFIS is paying for Christian, plus one open
   position in Peter's Group [4] and one position in Kars's group [5].

### PaNOSC and ExPANDS
   PaNOSC[6] and ExPaNDS[7] are two sister projects, mostly on meta
   data catalogues, analysis services and the European Open Science
   Cloud [8][9]. While PaNOSC partners are all ESFRI (Like EU-XFEL),
   the ExPaNDS partners are national Photon and Neutron facilities.
   Within PaNOSC we are only a linked third party but for ExPaNDS we
   provide the Project Manager (Sophie) and the Project Coordinator
   (myself). From both projects we got some money left which allows us
   to hire a cloud expert, supporting Michael's and Yves's team in
   running high level services on our Cloud Infrastructure.

### Finally : Getting ill in home office
{% if page.internal == "true" %}
   In the unlikely event of getting ill while in home office, the
   normal procedures apply. However, when reporting ill, either
   through go.desy.de or by sending an e-mail to itsec, please don't
   forget to keep me updated (best is office@ric.solutions). The same
   it true when reporting back to work or to home office. On a similar
   note: Sabine is asking you to send your AU to V2 directly to avoid
   delays, as she might be in home office once in awhile.
{% else %}
 This information is not publicly available.
{% endif %}

1. [HIP](https://www.helmholtz.de/en/research/information-data-science/helmholtz-imaging-platform-hip/)
2. [HIFIS](https://www.hifis.net/)
6. [PaNOSC](https://www.panosc.eu/)
7. [ExPaNDS](https://expands.eu/)
8. [EOSC](https://ec.europa.eu/research/openscience/index.cfm?pg=open-science-cloud)
9. [EOSC Portal](https://www.eosc-portal.eu/)

