---
layout: default-desy
title: "RIC Welcome" 
master: true
---

# RIC's Dashboard

<!--
         NEWS SECTION
-->

<div class="title-render">

{% include catagories-columns.html %}
<!--
{% include block_header.html cat="Material" %}
{% include material.html %}

{% include block_header.html cat="Event" %}
{% include events.html %}
-->

</div>
<br>

{% include xdc-infographics.html %}

<br>

<div class="title-render">
<h2>Our Projects</h2>
Please contact us at <span style="font-style: italic; color: orange;">office at ric.solutions</span> on questions or suggestions.
{% include projects-columns-grid.html %}
</div>


<br>

<div class="title-render">
<h2>The Team</h2>
<table>
<tr>
<td>
  <img style="width: 300px" src="/assets/images/sketch-people.jpg">
</td>
<td>
<!--
<ul style="list-style-type: none;">
{% for member in site.team %}
  <li> <a href="/team-index"> {{ member.name }} is a {{ member.position }} </a> </li>
{% endfor %}
</ul>
-->
Our team is composed of project coordinators and managers, software designers and developers, experts for graphical applications, imaging and AI as well as HPC and Cloud middleware and application architects.
<br>
<a style="color: orange" href="/team-index">More ...</a>
<!-- BEGIN TABLE 
<table style="border: 0px">
{% for member in site.team %}
  <tr>
  <td style="padding-right:5px;">
     <a href="/team-index"> {{ member.name }} </a>
  </td>
  <td style="color: orange">
    {{ member.position }}
  </td>
  </tr>
{% endfor %}
</table>
 END TABLE -->

</td>
</tr>
</table>
</div>
