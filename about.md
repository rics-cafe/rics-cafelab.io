---
layout: page
title: About
permalink: /about/
master: true
---
Research and Innovation in Computing (RIC) is a subdivision of the DESY IT devision. Its main objective is to contribute to IT development and IT infrastructure projects, whenever external stakeholders are involved.
At the time being RIC overviews projects with the German Bundesministerium für Bildung und Forschung (BMBF), the Helmholtz Alliance (HGF) and the European Commission. 
Within those projects or initiatives, we provide the coordination, the project management or we take over responsibilities for work package or tasks. In other projects we contribute with manpower to the description of work.
Please consult our [project summary](/projects-index.html) pages for further details.
Don't hesitate to contact us (office at ric.solutions) in case of questions or maybe you want to propose a collaboration. 

