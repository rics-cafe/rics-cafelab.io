---
layout: project
name: PaNOSC 
stakeholder: EC,H2020
funding_program: H2020
description: "Improving catalogue and analysis services for the Photon and Neutron Community"
picture: "/assets/images/logo-panosc.jpg"
contact: Michael, Patrick
---
The Photon and Neutron Open Science Cloud (PaNOSC) is a European project for making FAIR data a reality in 6 European Research Infrastructures (RIs), developing and providing services for scientific data and connecting these to the European Open Science Cloud (EOSC).

### Objectives

* Participate in the construction of the EOSC by linking with the e-infrastructures and other ESFRI clusters.
    Make scientific data produced at Europe’s major Photon and Neutron sources fully compatible with the FAIR principles.
* Generalise the adoption of open data policies, standard metadata and data stewardship from 15 photon and neutron RIs and physics institutes across Europe
* Provide innovative data services to the users of these facilities locally and the scientific community at large via the European Open Science Cloud (EOSC).
* Increase the impact of RIs by ensuring data from user experiments can be used beyond the initial scope.
* Share the outcomes with the national RIs who are observers in the proposal and the community at large to promote the adoption of FAIR data principles, data stewardship and the EOSC.
