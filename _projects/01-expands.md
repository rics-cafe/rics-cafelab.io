---
layout: project
name: ExPaNDS
stakeholder: EC, H2020, 5B
funding_program: H2020
description: "Improving catalogue and analysis services for the Photon and Neutron Community"
picture: "/assets/images/logo-expands.png"
contact: "Sophie"
---

The European Open Science Cloud (EOSC) Photon and Neutron Data Service (ExPaNDS) project will expand, accelerate and support the data management and data services provided through the EOSC for major national Photon and Neutron Research Infrastructures (PaN RIs) in delivering world-leading science.

It is an ambitious project which will create enormous opportunities for the scientific community and through their findings for humankind across the globe. This will be achieved through the collaboration and federation of 10 national PaN RIs from across Europe based on EOSC services in partnership with EGI, and will become a game-changer for the scientific user community.

ExPaNDS’s will make the majority of PaN RIs data ‘open’ following the FAIR principles (Findable, Accessible, Interoperable, Reusable) according to the user’s needs, and to harmonise efforts to migrate facility’s data analysis workflows to EOSC platforms enabling them to be shared in a uniform way.

ExPaNDS therefore seeks to:

    Enable EOSC services and to provide coherent FAIR data services to the scientific users of national Photon and Neutron sources
    Connect national PaN RIs through a platform of data analysis as a service for users from research institutes universities, industry etc.
    Develop and maintain a catalogue of data and analysis software for Photon and Neutron data
    Gather feedback and cooperate with the EOSC governance bodies to improve the EOSC and develop standard relationships between scientific publications, Photon and Neutron scientific dataset (raw data), experimental reports, instruments and authors (via ORCID) 

This project will standardise and link all relevant catalogues to ensure that any scientific research communities have access to both the raw data collected that is linked to their session(s) at these facilities, and the relevant peer review articles produced as a direct result of their usage.

In order to do this, ExPaNDS will develop a common ontology for all the elements of these catalogues, a roadmap for the back-end architecture and functionalities (including APIs) and a powerful taxonomy strategy in line with the requirement of the EOSC user community. 
