---
layout: project
name: AI@DESY
stakeholder: DESY
funding_program: Various
description: "Loose contact of the various AI initiaties at DESY"
picture: "/assets/images/logo-ai.png"
external_link: "https://ai.desy.de"
permalink: /projects/aai
contact: Philipp
---
One of team, Philipp, is coordinating efforts around AI at DESY. As a first measure he is orginizing frequent presentations, inviting internal and external scientists.  An [IA @ DESY web space]( {{  page.external_link }} ) is in preparation.
