---
layout: project
name: HIFIS
stakeholder: Helmholtz
funding_program: Incubator
description: "Building a HGF wide IT infrastructure"
picture: "/assets/images/logo-hifis.png"
contact: Uwe
---
 HIFIS aims to ensure an excellent information environment for outstanding research in all Helmholtz research fields and a seamless and performant IT-infrastructure connecting knowledge from all centres. It will build a secure and easy-to-use collaborative environment with efficiently accessible ICT services from anywhere. HIFIS will also support the development of research software with a high level of quality, visibility and sustainability.

To achieve this, HIFIS works in three Competence Clusters which are distributed throughout different Helmholtz research centers, a Cloud Services Cluster (a federated platform for proven first class cloud services), a Backbone Services Cluster (high-performance trusted network infrastructure with unified basic services) and a Software Services Cluster (platform, training and support for high-quality, sustainable software development). 

