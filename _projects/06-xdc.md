---
layout: project
name: eXtreme DataCloud
permalink: /projects/xdc
stakeholder: EC, H2020
funding_program: H2020
description: "Partical and Astronomy Cluster"
picture: "/assets/images/logo-xdc.png"
contact: Paul
---
The eXtreme DataCloud (XDC) is a EU H2020 funded project aimed at developing scalable technologies for federating storage resources and managing data in highly distributed computing environments. The services provided will be capable of operating at the unprecedented scale required by the most demanding, data intensive, research experiments in Europe and Worldwide.

XDC will be based on existing tools, whose technical maturity is proved and that the project will enrich with new functionalities and plugins already available as prototypes (TRL6+) that will be brought at the production level (TRL8+) at end of XDC.
The targeted platforms are the current and next generation e-Infrastructures deployed in Europe, such as the European Open Science Cloud (EOSC), the European Grid Infrastructure (EGI), the Worldwide LHC Computing Grid (WLCG) and the computing infrastructures funded by other public and academic initiatives.

The main high-level topics addressed by the project include:

* federation of storage resources with standard protocols
* smart caching solutions among remote locations
* policy driven data management based on Quality of Service
* data lifecycle management
* metadata handling and manipulation
* data preprocessing and encryption during ingestion
* optimized data management based on access patterns.

All the developments will be community-driven and tested against real life use cases provided by the consortium partners representing research communities belonging to a variety of scientific domains: Life Science, Astrophysics, High Energy Physics, Photon Science and Clinical Research.
The XDC project aims at opening new possibilities to scientific research communities in Europe and worldwide by supporting the evolution of e-Infrastructure services for Exascale data resources.
The XDC software will be released as Open Source platforms available for general exploitation.



