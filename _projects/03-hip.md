---
layout: project
name: HIP
stakeholder: Helmholtz
funding_program: Incubators
description: "HGF Common Imagin Platform"
picture: "/assets/images/logo-hip-small.jpg"
contact: Philipp
---
A significant amount of research data produced within the Helmholtz Association are imaging data. In order to obtain such data, the association has a unique collection of imaging modalities at its disposal, ranging from nanoscale to global observations. 
Imaging science is an enabling technology used in all research fields of the association.

Although  being  an  essential  component  of  research  at  all  centers  of  the  Helmholtz  Association,  imaging  science is not one of the central research topics of the association. Nevertheless, there exists a rich portfolio of expertise covering research on novel imaging modalities, experimental work using large-scale research faci-lities, expertise in mathematics and computer science related to imaging, as well as research in image analysis within specific fields of application.

The  Helmholtz  Imaging  Platform  HIP  aims  to  leverage  this  potential,  enabling  synergies  across  imaging  modalities and imaging applications. 

The platform consists of two components: 
* (i) a HIP Core Team providing scientific  support  and  operating  a  technological  platform  supporting  imaging  science  within  the  Helmholtz  Association. The Core Team further organizes the 
* (ii) HIP Imaging Network, comprising experts from all related disciplines within and outside of the Helmholtz Association. 

An essential mode of operation of HIP is a funding mechanism for short-term cross-domain projects, which may follow high-risk, cutting edge ideas, or bridge the gap from an early implementation towards a profes-sional  software  tool.  These  projects  have  a  seed  character  and  shall  develop  prototypes  solving  problems  in imaging sciences occurring within the application of imaging at the centers. Following a strategy of agile development, these solutions will further be explored by the imaging community at the Helmholtz Centers.

Overall, HIP aims at establishing the Helmholtz Association as a leading provider, developer and scientific user of cutting-edge imaging technology. 
