---
layout: project
name: MDLMA
stakeholder: BMBF
funding_program: National 
description: "Multi-task Deep Learning for Large-scale Multimodal Biomedical Image Analysis"
picture: "/assets/images/logo-mdlma.png"
contact: Philipp
---
MDLMA, acronym for ‘Multi-task Deep Learning for Large-scale Multimodal Biomedical Image Analysis’, is a joint research project of the Helmholtz-Zentrum Geesthacht (HZG), the Deutsche Elektronen-Synchrotron DESY, the University of Lübeck (UzL) and the company Syntellix AG. It is funded by the Federal Ministry of Education and Research (BMBF), grant number 031L0202A. 

The project is lead and coordinated by Prof. Dr. Regine Willumeit-Römer and Dr. Julian Moosmann. 


