---
layout: project
name: Teaching at HTW Berlin 
stakeholder: DESY
funding_program: DESY
description: "We regularly teach students at the HTW Berlin in Big Data and Analytics in Science."
picture: "/assets/images/logo-HTW-Berlin.jpg"
external_link: "https://www.htw-berlin.de/"
contact: Patrick
---
Members and friends of RIC are frequently teaching at the HTW Berlin.

{% include teaching.html %}


