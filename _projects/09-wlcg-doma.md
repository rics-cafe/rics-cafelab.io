---
layout: project
name: Doma
stakeholder: WLCG
funding_program: HEP, International 
description: "Set of WLCG DOMA sub-projects Data Management with a focus on the medium/long term evolution"
picture: "/assets/images/WLCG-Logo-cube-transparent.png"
external_link: "https://twiki.cern.ch/twiki/bin/view/LCG/DomaActivities"
contact: Paul
---
The [WLCG DOMA project]({{ page.external_link }}) consists of several activities in the area of Data Organization, Management and Access, with a focus on the medium/long term evolution.
