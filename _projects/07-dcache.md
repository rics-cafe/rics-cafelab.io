---
layout: project
name: dCache.org
stakeholder: DESY,Fermi,NDGF
funding_program:
description: "Storage Management"
picture: "/assets/images/logo-dcache.png"
contact: Tigran
---
The goal of this project is to provide a system for storing and retrieving huge amounts of data, distributed among a large number of heterogenous server nodes, under a single virtual filesystem tree with a variety of standard access, authentication and autorization methods. 

Depending on the Persistency Model, dCache provides methods for exchanging data with backend (tertiary) Storage Systems as well as space management, pool attraction, dataset replication, hot spot determination and recovery from disk or node failures. Connected to a tertiary storage system, the cache simulates unlimited direct access storage space. Data exchanges to and from the underlying HSM are performed automatically and invisibly to the user. Beside HEP specific protocols, data in dCache can be accessed via NFSv4.1 (pNFS) as well as through WebDav.
<br>
<a href="https://www.dcache.org/manuals/dCache-Whitepaper.pdf">Read the dCache Whitepaper</a>
