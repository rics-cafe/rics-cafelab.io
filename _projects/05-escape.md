---
layout: project
name: ESCPAPE
stakeholder: EC, H2020
funding_program: H2020
description: "Partical and Astronomy Cluster"
picture: "/assets/images/logo-escape.png"
contact: Paul, Aleem
---

Establish a single collaborative cluster of next generation European Strategy Forum on Research Infrastructures (ESFRI) facilities in the area of astronomy- and accelerator-based particle physics in order to implement a functional link between the concerned ESFRI projects and European Open Science Cloud (EOSC).
Why ESCAPE

ESCAPE (European Science Cluster of Astronomy & Particle physics ESFRI research infrastructures) brings together the astronomy, astroparticle and particle physics communities. With this, ESCAPE puts together a cluster with ESFRI projects with aligned challenges of data-driven research, with demonstrated capabilities in addressing various stages of data workflow and concerned with fundamental research through complementary approaches.

ESCAPE aims to produce versatile solutions, with great potential for discovery, to support the implementation of EOSC thanks to open data management, cross-border and multi-disciplinary open environment, according to FAIR (Findable, Accessible, Interoperable and Reusable) principles. The ESCAPE foundations lay on the capacity building of the ASTERICS project work towards enabling interoperability between the facilities, minimising fragmentation, encouraging cross-fertilisation and developing joint multiwavelength/multi-messenger capabilities in astronomy, astrophysics and particle astrophysics communities.
What are ESCAPE outputs?

A Big-Science domain-specific “EOSC cell” to be connected/integrated in the global EOSC infrastructure and to help setting up the related services. 

### Such an EOSC cell is composed of five main components:

* ESCAPE Software Repository: the repository of scientific software services of the research infrastructures concerned by the ESCAPE.
* ESCAPE Science Platform: a flexible science platform for the analysis of open access data.
* ESCAPE Virtual Observatory: astronomical high-level products archive and related services.
* ESCAPE Data Lake Cloud Services: a scalable federated data infrastructure as the basis of an open access data service for the ESFRI projects within ESCAPE and concerned by Exabyte-scale data volumes.
* ESCAPE Citizen Science: an open gateway dedicated to the public through Citizen Science and communication actions.

ESCAPE provides deep training, education and capacity building programmes for the new generation of scientists and engineers that fully exploit ESFRI and EOSC facilities, to ensure the requirements and service features are properly understood and uptake.

Know more about ESCAPE services here.

### ESCAPE main goals?

* Improve access to data and tools to unlock innovation for the society at large.
* Provide data with FAIR principles to increase researchers’ efficiency thanks to scientific data interoperability and establish new methodological approaches and rules for quality certified data and science tools sharing.
* Build a European cross-border and multi-disciplinary open innovation environment for research data, knowledge and services, while connecting EOSC and ESFRI.
* Facilitate interdisciplinary and networked research between different sciences, through research infrastructure ecosystem and by supporting data publishing, analytics, computational capacity, virtual analysis environments and workflow systems.
* Create of economies of scale, through the adoption of common approaches for data management
* Educate and train the scientific and wider user communities, to ensure the up-take of ESCAPE’s results.

### Who can join ESCAPE?

    e-Infrastructures
    ESFRI projects
    Industry (namely SMEs)
    EOSC Governance
    Policy Bodies
    Pan-European Research Organisations

### Join the movement and pre-register to our free services!

* Follow ESCAPE on [Twitter](https://twitter.com/ESCAPE_EU)
* Connect with ATMOSPHERE on [LinkedIn](https://www.linkedin.com/feed/)
* Pre-register on ESCAPE website and subscribe to the [ESCAPE newsletter](http://escape.trust-itservices.eu/user/login)


